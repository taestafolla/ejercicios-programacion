#include<stdio.h>
#define DOLAR 20.15

double dolarPeso(double cantidadDolar){
    if(cantidadDolar >= 0) return DOLAR * cantidadDolar; 
    else return -1;
}

int main(){
    double valorEnDolares = 0;
    double valorEnPesos = 0;
    printf("Ingrese la cantidad en dolares que desea transformar a pesos mexicanos \n");
    scanf("%lf", &valorEnDolares);
    valorEnPesos = dolarPeso(valorEnDolares);
    if(valorEnPesos != -1) printf("%.2lf dolares a peso mexicano es %.2lf\n", valorEnDolares, valorEnPesos );
    else printf("El programa no reconoce valores negativos\n");
}
