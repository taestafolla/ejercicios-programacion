#include<stdio.h>
#define N_INICIO 0
#define N_FIN 100
#define PAR 2
void imprimePares(){
    for(int i = N_INICIO; i <= N_FIN; i++){
        if(i % PAR == 0){
            printf("%d, ", i);
        }
    }
    printf("\n");

}
int main(){
    printf("Numeros pares entre 0 y 100\n");
    imprimePares();
    return 0;
}
