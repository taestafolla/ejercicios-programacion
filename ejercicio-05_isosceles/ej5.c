#include<stdio.h>

double perimetroIsoceles(double unLado, double dosLados){
    if(unLado > 0 && dosLados > 0) return unLado + 2 * dosLados;
    else return -1;

}
int main(){
    double unLado = 0;
    double dosLados = 0;
    double perimetro = 0;
    printf("Programa para el calculo del perimetro de un triangulo isoceles\n");
    printf("Ingresa la longitud de su lado unico en centimetros: ");
    scanf("%lf", &unLado);
    printf("Ingresa la longitud de su lado doble en centimetros: ");    
    scanf("%lf", &dosLados);
    perimetro = perimetroIsoceles(unLado, dosLados);
    if(perimetro != -1) printf("El perimetro del triangulo es: %.2lf [cm]\n", perimetro);
    else printf("El programa solo permite el uso de valores mayores a 0\n");
    return 0;
}
