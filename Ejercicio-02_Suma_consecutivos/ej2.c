
#include<stdio.h>
#define INICIO 1
#define FIN 50

int sumaConsecutiva(int numero){
    if(numero >= INICIO && numero <= FIN){
        if(numero == INICIO){
            return INICIO;
        }
        else{
            return numero + sumaConsecutiva(numero-1);
        }
    }
    else{
        printf("El programa no implementa una solucion para el numero seleccionado\n");
        return -1;
    }
}

int main(){
    int numero = 0;
    int resultadoSuma = 0;
    printf("Ingresa un numero entre el 1 y el 50\n");
    scanf("%d", &numero);
    resultadoSuma = sumaConsecutiva(numero);
    if(resultadoSuma != -1) printf("El resultado de la suma es %d\n", resultadoSuma);
    return 0;
}
