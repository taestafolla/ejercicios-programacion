#include<stdio.h>

double perimetroEscaleno(double ladoUno, double ladoDos, double ladoTres){
    if(ladoUno > 0 && ladoDos > 0 && ladoTres > 0) return ladoUno + ladoDos + ladoTres;
    else return -1;
}

int main(){
    double ladoUno = 0, ladoDos = 0, ladoTres = 0, perimetro = 0;
    printf("Programa para el calculo del perimetro de un triangulo escaleno\n");
    printf("Ingresa la longitud del lado uno en centimetros: ");
    scanf("%lf", &ladoUno);
    
    printf("Ingresa la longitud del lado dos en centimetros: ");
    scanf("%lf", &ladoDos);
    
    printf("Ingresa la longitud del lado tres en centimetros: ");
    scanf("%lf", &ladoTres);
    perimetro = perimetroEscaleno(ladoUno, ladoDos, ladoTres);
    if(perimetro != -1) printf("El perimetro del triangulo es: %.2lf\n [cm]", perimetro);
    else printf("El programa solo permite el uso de valores mayores a 0\n");
    return 0;
}
