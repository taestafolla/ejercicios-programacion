#include<stdio.h>
double perimetroEquilatero(double lado){
    if(lado > 0) return lado*3;
    else return -1;
}

int main(){
    double lado = 0;
    double perimetro = 0;
    printf("Programa para el calculo del perimetro de un triangulo equilatero\n");
    printf("Ingresa la longitud de su lado en centimetros: ");

    scanf("%lf", &lado);
    perimetro = perimetroEquilatero(lado); 
    if(perimetro != -1) printf("El perimetro del triangulo es: %.2lf\n", perimetro);
    else printf("El programa solo reconoce valores mayores a 0\n");
    return 0;
}
